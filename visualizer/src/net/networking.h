#ifndef NET_NETWORKING_H
#define NET_NETWORKING_H

#include "gfx/graphics.h"

#include <netinet/in.h>
#include <pthread.h>

struct UdpThreadData {
	/* constant variables */
	int socket;
	struct sockaddr_in address;

	/* mutable variables */
	pthread_mutex_t should_stop_m;
	pthread_mutex_t packet_count_m;
	pthread_mutex_t polarity_m;

	int should_stop;
	uint32_t packet_count;
	uint8_t polarity[GFX_DAVIS_FX2_Y][GFX_DAVIS_FX2_X][2]; /* accumulated */
};

struct Net {
	/* threading data here */
	pthread_t udp_tid;
	struct UdpThreadData udp;
};

/* starts the work threads */
int setup_net(struct Net* net, const char* caer_ip_address, int udp_port);
/* ends the work threads */
void setdown_net(struct Net* net);

/* function which returns how many packets have been processed since the last sync */
uint32_t udp_packet_count(struct Net* net);
/* function to sync the polarity data with the graphics state */
void sync_udp_with_graphics(struct Net* net, struct Graphics* graphics);

#endif
