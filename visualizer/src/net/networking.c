#include "net/networking.h"

#include "ext/nets.h"
#define PRINTF_LOG 1
#include "events/imu6.h"
#include "events/polarity.h"
#include "events/frame.h"
#include "events/special.h"

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void recv_error() {
	fprintf(stderr, "recv error: %d\n", errno);
}

/* todo, to decrease latency, only have this thread save data into a buffer */
/* and then have sync() parse the data */
void* udp_thread(void* thread_data) {
	struct UdpThreadData* data = (struct UdpThreadData*)thread_data;

	/* todo: send a command to the server specifying maximum udp packet size */
	size_t packet_buffer_size = 1024 * 1024;
	uint8_t* packet_buffer = malloc(packet_buffer_size);
	if(packet_buffer == NULL) return NULL;

	pthread_mutex_lock(&data->should_stop_m);
	int should_stop = data->should_stop;
	pthread_mutex_unlock(&data->should_stop_m);

	while(!should_stop) {
		/* receive the event header packet */
		ssize_t result = recv(data->socket, packet_buffer, packet_buffer_size, 0);
		if(result <= 0) {
			recv_error();
			break;
		}

		
		/* unpack the event header packet data */
		caerEventPacketHeader header = (caerEventPacketHeader)packet_buffer;
		uint16_t event_type     = caerEventPacketHeaderGetEventType    (header);
		uint32_t event_size     = caerEventPacketHeaderGetEventSize    (header);
		uint32_t event_capacity = caerEventPacketHeaderGetEventCapacity(header);
		uint32_t event_valid    = caerEventPacketHeaderGetEventValid   (header);
		
		/* receive the event data packet */
		/* gfx_assert(packet_buffer_size - sizeof(struct caer_event_packet_header) >= event_capacity * event_size); */
		if(!recvUntilDone(data->socket, packet_buffer + sizeof(struct caer_event_packet_header), event_capacity * event_size)) {
			recv_error();
			break;
		}

		/* parse the event data packet */
		if(event_type == POLARITY_EVENT && event_valid > 0) {
			/* update local polarity data */
			uint32_t event_number = caerEventPacketHeaderGetEventNumber(header);
			caerPolarityEventPacket polarity_packet = (caerPolarityEventPacket)packet_buffer;
			uint32_t i;
			for(i = 0; i < event_number; ++i) {
				caerPolarityEvent polarity_event = caerPolarityEventPacketGetEvent(polarity_packet, i);
				if(!caerPolarityEventIsValid(polarity_event)) continue;
				//uint32_t polarity_ts = caerPolarityEventGetTimestamp(polarity_event);
				bool polarity_polarity = caerPolarityEventGetPolarity(polarity_event);
				uint16_t polarity_pos[2] = {
					caerPolarityEventGetX(polarity_event),
					caerPolarityEventGetY(polarity_event)
				};

				/* it turns out that caerPolarityEventIsValid doesn't check these..? */
				if(polarity_pos[0] >= GFX_DAVIS_FX2_X) continue;
				if(polarity_pos[1] >= GFX_DAVIS_FX2_Y) continue;

				uint16_t x = polarity_pos[0]; 
				uint16_t y = polarity_pos[1];
				static uint8_t increment = 1;
				
				pthread_mutex_lock(&data->polarity_m);
				if(polarity_polarity) {
					/* update green channel */
					if(data->polarity[y][x][1] + increment <= (uint8_t)(-1)) {
						data->polarity[y][x][1] += increment;
					}
				} else {
					/* update red channel */
					if(data->polarity[y][x][0] + increment <= (uint8_t)(-1)) {
						data->polarity[y][x][0] += increment;
					}
				}
				pthread_mutex_unlock(&data->polarity_m);
			}
		}

		/* increment the packet counter */
		pthread_mutex_lock(&data->packet_count_m);
		data->packet_count++;
		pthread_mutex_unlock(&data->packet_count_m);

		/* check if we're requested to stop the thread */
		pthread_mutex_lock(&data->should_stop_m);
		should_stop = data->should_stop;
		pthread_mutex_unlock(&data->should_stop_m);
	}
	
	free(packet_buffer);
	return NULL;
}

/* http://stackoverflow.com/questions/4181784/how-to-set-socket-timeout-in-c-when-making-multiple-connections*/ 
int setup_net(struct Net* net, const char* caer_ip_address, int udp_port) {
	/* udp */
	net->udp.socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if(net->udp.socket < 0) {
		fprintf(stderr, "failed to create udp socket.\n");
		return 0;
	}

	net->udp.address.sin_family = AF_INET;
	net->udp.address.sin_port = htons(udp_port);
	inet_aton(caer_ip_address, &net->udp.address.sin_addr); 

	if(bind(net->udp.socket, (struct sockaddr*)&net->udp.address, sizeof(net->udp.address)) < 0) {
		fprintf(stderr, "failed to listen on udp socket.\n");
		return 0;
	}

	net->udp.should_stop = 0;
	memset(net->udp.polarity, 0, sizeof(net->udp.polarity));

	/* start the thread here */
	if(pthread_mutex_init(&net->udp.should_stop_m, NULL) != 0 || 
	   pthread_mutex_init(&net->udp.packet_count_m, NULL) != 0 ||
	   pthread_mutex_init(&net->udp.polarity_m, NULL) != 0) 
	{
		fprintf(stderr, "pthread_mutex_init failed.\n");
		return 0;
	}

	if(pthread_create(&net->udp_tid, NULL, udp_thread, &net->udp) != 0) {
		fprintf(stderr, "pthread_create failed.\n");
		return 0;
	}
	return 1;
}

void setdown_net(struct Net* net) {
	/* attempt to signal the thread to stop */
	pthread_mutex_lock(&net->udp.should_stop_m);
	net->udp.should_stop = 1;
	pthread_mutex_unlock(&net->udp.should_stop_m);

	/* wait for the thread to finish */
	pthread_join(net->udp_tid, NULL);

	/* cleanup */
	pthread_mutex_destroy(&net->udp.should_stop_m);
	pthread_mutex_destroy(&net->udp.packet_count_m);
	pthread_mutex_destroy(&net->udp.polarity_m);

	close(net->udp.socket);
}

uint32_t udp_packet_count(struct Net* net) {
	uint32_t packet_count;
	pthread_mutex_lock(&net->udp.packet_count_m);
	packet_count = net->udp.packet_count;
	pthread_mutex_unlock(&net->udp.packet_count_m);
	return packet_count;
}

void sync_udp_with_graphics(struct Net* net, struct Graphics* graphics) {
	uint8_t polarity[GFX_DAVIS_FX2_Y][GFX_DAVIS_FX2_X][2];

	/* copy and reset the polarity data */
	pthread_mutex_lock(&net->udp.polarity_m);
	memcpy(polarity, net->udp.polarity, sizeof(polarity));
	memset(net->udp.polarity, 0, sizeof(net->udp.polarity));
	pthread_mutex_unlock(&net->udp.polarity_m);

	/* reset the packet counter */
	pthread_mutex_lock(&net->udp.packet_count_m);
	net->udp.packet_count = 0;
	pthread_mutex_unlock(&net->udp.packet_count_m);

	/* sync the graphics state */
	size_t y, x;
	for(y = 0; y < GFX_DAVIS_FX2_Y; ++y) {
		for(x = 0; x < GFX_DAVIS_FX2_X; ++x) {
			static uint8_t rg[2];
			rg[0] = polarity[y][x][0] != 0;
			rg[1] = polarity[y][x][1] != 0;

			/* do not modify if there aren't any events */
			if(!rg[0] && !rg[1]) continue;
			
			/* clear the blue channel and write RED, GREEN, or YELLOW */
			graphics->frame_data[y][x][2] = 0;
			graphics->frame_data[y][x][0] = rg[0] * 255;
			graphics->frame_data[y][x][1] = rg[1] * 255;
		}
	}
}
