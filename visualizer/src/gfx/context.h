#ifndef GFX_CONTEXT_H
#define GFX_CONTEXT_H

/* targetting opengl 3.3; glsl 330 */
#include <gl3w/gl3w.h>

#ifdef gl 
#error this macro must not be defined, since we use it as a wrapper around all opengl calls
#else
#include "gfx/assert.h"
#endif 

#ifndef NDEBUG
#define gl(OPENGL_CALL) \
	gl##OPENGL_CALL
#else
#define gl(OPENGL_CALL) \
	gl##OPENGL_CALL; \
	gfx_assert(glGetError() == GL_NO_ERROR);
#endif 

#endif
