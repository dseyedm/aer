#include "gfx/shader.h"

extern inline void create_shader(struct Shader* shader, GLenum type);
extern inline void destroy_shader(struct Shader* shader);

extern inline void create_program(struct Program* program);
extern inline void destroy_program(struct Program* program);

extern inline void set_attribute_handle(struct Program program, const char* id, GLuint handle);

extern inline void bind_program(struct Program program);
extern inline void unbind_program();

extern inline int get_uniform_count(struct Program program);
extern inline int get_attribute_count(struct Program program);

extern inline struct Uniform get_uniform(struct Program program, const char* id); 
extern inline struct Attribute get_attribute(struct Program program, const char* id);

extern inline void enable_attribute(struct Attribute attribute);
extern inline void disable_attribute(struct Attribute attribute);

extern inline void set_attribute_ptr(struct Attribute attribute, GLenum type, GLuint components);
extern inline void set_attribute1f(struct Attribute attribute, float v);
extern inline void set_attribute2f(struct Attribute attribute, const vec2 v);
extern inline void set_attribute3f(struct Attribute attribute, const vec3 v);
extern inline void set_attribute4f(struct Attribute attribute, const vec4 v);

/* ommitted set_uniform#[i/u](a), they are rarely used */

extern inline void set_uniform1i(struct Uniform uniform, int i);

extern inline void set_uniform1f(struct Uniform uniform, float v);
extern inline void set_uniform2f(struct Uniform uniform, const vec2 v);
extern inline void set_uniform3f(struct Uniform uniform, const vec3 v);
extern inline void set_uniform4f(struct Uniform uniform, const vec4 v);

/* ommitted set_uniform#fa, they are rarely used */

/* ommitted mat#x#, mat4x4 is mostly used */
extern inline void set_uniform4x4f(struct Uniform uniform, const mat4x4 v);

/* ommitted set_uniform#x#fa, they are rarely used */

#include <stdlib.h>
#include <string.h>

char* compile_shader(struct Shader shader, const char* source) {
	gl(ShaderSource(shader.handle, 1, &source, NULL));
	gl(CompileShader(shader.handle));
	GLint compiled = GL_FALSE;
	gl(GetShaderiv(shader.handle, GL_COMPILE_STATUS, &compiled));
	if(compiled != GL_TRUE) {
		GLint info_len;
		gl(GetShaderiv(shader.handle, GL_INFO_LOG_LENGTH, &info_len));
		char* info = malloc(sizeof(*info) * (info_len + 1));
		if(info == NULL) return info;
		/* the string that will be returned is NULL terminated */
		gl(GetShaderInfoLog(shader.handle, info_len, NULL, info));
		return info;
	}
	return NULL;
}

char* get_shader_src(struct Shader shader) {
	GLsizei src_len;
	gl(GetShaderiv(shader.handle, GL_SHADER_SOURCE_LENGTH, &src_len));
	char* src = malloc(sizeof(*src) * (src_len + 1));
	if(src == NULL) return src;
	/* the string that will be returned is NULL terminated */
	gl(GetShaderSource(shader.handle, src_len, NULL, src));
	return src;
}

char* link_program(struct Program program, struct Shader vertex, struct Shader fragment) {
	gl(AttachShader(program.handle, vertex.handle));
	gl(AttachShader(program.handle, fragment.handle));
	gl(LinkProgram(program.handle));
	GLint linked = GL_FALSE;
	gl(GetProgramiv(program.handle, GL_LINK_STATUS, &linked));
	if(linked != GL_TRUE) {
		GLint info_len;
		gl(GetProgramiv(program.handle, GL_INFO_LOG_LENGTH, &info_len));
		char* info = malloc(sizeof(*info) * (info_len + 1));
		/* the string that will be returned is NULL terminated */
		gl(GetProgramInfoLog(program.handle, info_len, NULL, info));
		gl(DetachShader(program.handle, vertex.handle));
		gl(DetachShader(program.handle, fragment.handle));
		return info;
	}
	gl(DetachShader(program.handle, vertex.handle));
	gl(DetachShader(program.handle, fragment.handle));
	return NULL;
}

int get_uniforms(struct Program program, struct UniformName** uniforms_out, size_t* size_out) {
	GLint count, max_len;
	count = get_uniform_count(program);
	gl(GetProgramiv(program.handle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &max_len));
	struct UniformName* uniforms = malloc(sizeof(*uniforms) * count);
	if(uniforms == NULL) return 0;
	GLint i;
	char temp_name[max_len + 1];
	for(i = 0; i < count; ++i) {
		GLsizei len;
		GLint size;
		GLenum type;
		/* the string that will be returned is NULL terminated */
		gl(GetActiveUniform(program.handle, i, max_len, &len, &size, &type, temp_name));
		uniforms[i].handle = gl(GetUniformLocation(program.handle, temp_name));
		uniforms[i].name = malloc(sizeof(*uniforms[i].name) * (len + 1));
		strcpy(uniforms[i].name, temp_name);
	}
	*uniforms_out = uniforms;
	*size_out = count;

	return 1;
}

int get_attributes(struct Program program, struct AttributeName** attributes_out, size_t* size_out) {
	GLint count, max_len;
	count = get_attribute_count(program);
	gl(GetProgramiv(program.handle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &max_len));
	struct AttributeName* attributes = malloc(sizeof(*attributes) * count);
	if(attributes == NULL) return 0;
	char temp_name[max_len + 1];
	GLint i;
	for(i = 0; i < count; ++i) {
		GLsizei len;
		GLint size;
		GLenum type;
		/* the string that will be returned is NULL terminated */
		gl(GetActiveAttrib(program.handle, i, max_len, &len, &size, &type, temp_name));
		attributes[i].handle = gl(GetAttribLocation(program.handle, temp_name));
		attributes[i].name = malloc(sizeof(*attributes[i].name) * (len + 1));
		strcpy(attributes[i].name, temp_name);
	}
	*attributes_out = attributes;
	*size_out = count;

	return 1;
}
