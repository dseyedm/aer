#ifndef GFX_GRAPHICS_H
#define GFX_GRAPHICS_H

#include "gfx/context.h"
#include "gfx/assert.h"
#include "gfx/shader.h"
#include "gfx/texture.h"
#include "gfx/vertex_array.h"
#include "gfx/vertex_buffer.h"
#include "gfx/mesh.h"

/* make sure to include gfx/context.h before glfw */
#include <GLFW/glfw3.h>

#define GFX_DAVIS_FX2_X 240
#define GFX_DAVIS_FX2_Y 180

struct Graphics {
	/* window options */
	GLFWwindow* window;
	/* shader programs */
	struct Program display;
	/* display settings */
	struct Attribute quad_cs;    /* clip space quad */
	struct Uniform scale,        /* vec2 */
	               offset,       /* vec2 */ 
	               blit_texture, /* sampler2D */ 
		       uv_offset,    /* vec2 */
	               uv_scale,     /* vec2 */ 
	               color;        /* vec4 */
	/* texture data */
	struct Texture2D frame;
	/* 2d array of rgb */
	uint8_t frame_data[GFX_DAVIS_FX2_Y][GFX_DAVIS_FX2_X][3];
};

/* always pair init_graphics with terminate_graphics */
int init_graphics();
void terminate_graphics();

/* set default values for Graphics; make sure to setdown before calling if it has been setup before */
void reset_graphics(struct Graphics* graphics);

/* always pair setup_graphics with setdown_graphics */
int setup_graphics(struct Graphics* graphics);
void setdown_graphics(struct Graphics* graphics);

void graphics_update_frame(struct Graphics* graphics);

int render_graphics(struct Graphics* graphics);

#endif
