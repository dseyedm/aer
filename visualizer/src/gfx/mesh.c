#include "gfx/mesh.h"
#include "gfx/assert.h"

#include <stdlib.h>

void create_mesh2d(struct Mesh2D* mesh) {
	reset_mesh2d(mesh);

	create_vao(&mesh->vao);
	create_vbo(&mesh->elements_vbo);
	create_vbo(&mesh->vertices_vbo);
	create_vbo(&mesh->uvs_vbo);
}

void destroy_mesh2d(struct Mesh2D* mesh) {
	destroy_vao(&mesh->vao);
	destroy_vbo(&mesh->elements_vbo);
	destroy_vbo(&mesh->vertices_vbo);
	destroy_vbo(&mesh->uvs_vbo);

	reset_mesh2d(mesh);
}

void reset_mesh2d(struct Mesh2D* mesh) {
	mesh->vao.handle = (GLuint)(-1);
	mesh->elements_vbo.handle = (GLuint)(-1);
	mesh->vertices_vbo.handle = (GLuint)(-1);
	mesh->uvs_vbo.handle = (GLuint)(-1);
	mesh->elements = NULL;
	mesh->vertices = NULL;
	mesh->uvs = NULL;
	mesh->elements_size = 0;
	mesh->vertices_size = 0;
	mesh->uvs_size = 0;
}

void upload_mesh2d(struct Mesh2D* mesh) {
	bind_vao(mesh->vao);
	if(mesh->elements != NULL) {
		bind_element_vbo(mesh->elements_vbo);
		upload_element_vbo8(mesh->elements, mesh->elements_size);
		unbind_element_vbo();
		free(mesh->elements);
		mesh->elements = NULL;
	}
	if(mesh->vertices != NULL) {
		bind_array_vbo(mesh->vertices_vbo);
		upload_array_vbo2fa(mesh->vertices, mesh->vertices_size);
		unbind_array_vbo();
		free(mesh->vertices);
		mesh->vertices = NULL;
	}
	if(mesh->uvs != NULL) {
		bind_array_vbo(mesh->uvs_vbo);
		upload_array_vbo2fa(mesh->uvs, mesh->uvs_size);
		unbind_array_vbo();
		free(mesh->uvs);
		mesh->uvs = NULL;
	}
	unbind_vao();
}
	
void attach_mesh2d(struct Mesh2D* mesh, struct Attribute* list, size_t list_size) {
	gfx_assert(list_size && list_size <= 2);
	bind_vao(mesh->vao);
	/* vertices */
	if(list[0].handle != (GLuint)(-1)) {
		bind_array_vbo(mesh->vertices_vbo);
		enable_attribute(list[0]);
		set_attribute_ptr(list[0], GL_FLOAT, 2);
		unbind_array_vbo();
	}
	/* uvs */
	if(list_size >= 2 && list[1].handle != (GLuint)(-1)) {
		bind_array_vbo(mesh->uvs_vbo);
		enable_attribute(list[1]);
		set_attribute_ptr(list[1], GL_FLOAT, 2);
		unbind_array_vbo();
	}
	unbind_vao();
}

void render_mesh2d_elements(struct Mesh2D* mesh, GLenum mode) {
	bind_vao(mesh->vao);
	bind_element_vbo(mesh->elements_vbo);
	render_element_vbo8(mesh->elements_size, mode);
	unbind_element_vbo();
	unbind_vao();
}

void render_mesh2d_vertices(struct Mesh2D* mesh, GLenum mode) {
	bind_vao(mesh->vao);
	bind_array_vbo(mesh->vertices_vbo);
	render_array_vbo(mesh->vertices_size, mode);
	unbind_array_vbo();
	unbind_vao();
}
