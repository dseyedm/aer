#ifndef GFX_TEXTURE_H
#define GFX_TEXTURE_H

#include "gfx/context.h"

struct Texture2D {
	GLuint handle;
};

inline int active_texture(GLint unit) {
	gl(ActiveTexture(GL_TEXTURE0 + unit));
	return unit;
}

inline void create_texture2d(struct Texture2D* texture) {
	gl(GenTextures(1, &texture->handle));
}

inline void destroy_texture2d(struct Texture2D* texture) {
	gl(DeleteTextures(1, &texture->handle));
	texture->handle = (GLuint)(-1);
}

inline void bind_texture2d(struct Texture2D texture) {
	gl(BindTexture(GL_TEXTURE_2D, texture.handle));
}

inline void unbind_texture2d() {
	gl(BindTexture(GL_TEXTURE_2D, 0));
}

inline struct Texture2D bound_texture2d() {
	GLint bound;
	gl(GetIntegerv(GL_TEXTURE_BINDING_2D, &bound));
	struct Texture2D texture;
	texture.handle = bound > 0 ? (GLuint)bound : (GLuint)(-1);
	return texture;
}

inline void upload_texture2d(GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format) {
	gfx_assert(bound_texture2d().handle != (GLuint)(-1));
	gl(TexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, type, data));
}

inline void set_filter_texture2d(GLenum filter_mode) {
	gfx_assert(bound_texture2d().handle != (GLuint)(-1));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter_mode));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter_mode));
}

inline void set_wrap_texture2d(GLenum wrap_mode) {
	gfx_assert(bound_texture2d().handle != (GLuint)(-1));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode));
	gl(TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode));
}

#endif
