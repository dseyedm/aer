#ifndef GFX_VERTEX_BUFFER_H
#define GFX_VERTEX_BUFFER_H

#include "gfx/context.h"

#include <stdint.h>
#include <linmath/linmath.h>

struct Vbo {
	GLuint handle;
};

inline void create_vbo(struct Vbo* vbo) {
	gl(GenBuffers(1, &vbo->handle));
}

inline void destroy_vbo(struct Vbo* vbo) {
	gl(DeleteBuffers(1, &vbo->handle));
	vbo->handle = (GLuint)(-1);
}

inline void bind_element_vbo(struct Vbo vbo) {
	gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.handle));
}

inline void unbind_element_vbo() {
	gl(BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

inline struct Vbo bound_element_vbo() {
	GLint bound;
	gl(GetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &bound));
	struct Vbo vbo;
	vbo.handle = bound > 0 ? (GLuint)bound : (GLuint)(-1);
	return vbo;
}

inline void bind_array_vbo(struct Vbo vbo) {
	gl(BindBuffer(GL_ARRAY_BUFFER, vbo.handle));
}

inline void unbind_array_vbo() {
	gl(BindBuffer(GL_ARRAY_BUFFER, 0));
}

inline struct Vbo bound_array_vbo() {
	GLint bound;
	gl(GetIntegerv(GL_ARRAY_BUFFER_BINDING, &bound));
	struct Vbo vbo;
	vbo.handle = bound > 0 ? (GLuint)bound : (GLuint)(-1);
	return vbo;
}

inline void upload_element_vbo8(uint8_t* data, size_t size) {
	gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void upload_element_vbo16(uint16_t* data, size_t size) {
	gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void upload_element_vbo32(uint32_t* data, size_t size) {
	gl(BufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void upload_array_vbo1fa(float* data, size_t size) {
	gl(BufferData(GL_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void upload_array_vbo2fa(vec2* data, size_t size) {
	gl(BufferData(GL_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void upload_array_vbo3fa(vec3* data, size_t size) {
	gl(BufferData(GL_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void upload_array_vbo4fa(vec4* data, size_t size) {
	gl(BufferData(GL_ARRAY_BUFFER, sizeof(*data) * size, data, GL_STATIC_DRAW));
}

inline void render_element_vbo8(size_t size, GLenum mode) {
	gl(DrawElements(mode, size, GL_UNSIGNED_BYTE, (GLvoid*)0));
}

inline void render_element_vbo16(size_t size, GLenum mode) {
	gl(DrawElements(mode, size, GL_UNSIGNED_SHORT, (GLvoid*)0));
}

inline void render_element_vbo32(size_t size, GLenum mode) {
	gl(DrawElements(mode, size, GL_UNSIGNED_INT, (GLvoid*)0));
}

inline void render_array_vbo(size_t size, GLenum mode) {
	gl(DrawArrays(mode, 0, size));
}

#endif
