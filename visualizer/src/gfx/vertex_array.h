#ifndef GFX_VERTEX_ARRAY_H
#define GFX_VERTEX_ARRAY_H

#include "gfx/context.h"

struct Vao {
	GLuint handle;
};

inline void create_vao(struct Vao* vao) {
	gl(GenVertexArrays(1, &vao->handle));
}

inline void destroy_vao(struct Vao* vao) {
	gl(DeleteVertexArrays(1, &vao->handle));
}

inline void bind_vao(struct Vao vao) {
	gl(BindVertexArray(vao.handle));
}

inline void unbind_vao() {
	gl(BindVertexArray(0));
}

inline struct Vao bound_vao() {
	GLint bound;
	gl(GetIntegerv(GL_VERTEX_ARRAY_BINDING, &bound));
	struct Vao vao;
	vao.handle = bound > 0 ? (GLuint)bound : (GLuint)(-1); 
	return vao;
}

#endif
