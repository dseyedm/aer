#ifndef GFX_SHADER_H
#define GFX_SHADER_H

#include "gfx/context.h"
#include "gfx/vertex_array.h"

#include <linmath/linmath.h>

struct Shader {
	GLuint handle;
};

/* caller must free returned string */
char* compile_shader(struct Shader shader, const char* source);
/* caller must free returned string */
char* get_shader_src(struct Shader shader);

struct Program {
	GLuint handle;
};

/* caller must free returned string */
char* link_program(struct Program program, struct Shader vertex, struct Shader fragment);

struct Uniform {
	GLuint handle;
};

struct Attribute {
	GLuint handle;
};

struct UniformName {
	GLuint handle;
	char* name;
};

struct AttributeName {
	GLuint handle;
	char* name;
};

/* caller must free each .name and the array itself */
int get_uniforms(struct Program program, struct UniformName** uniforms_out, size_t* size_out);
/* caller must free each .name and the array itself */
int get_attributes(struct Program program, struct AttributeName** attributes_out, size_t* size_out);

inline int get_uniform_count(struct Program program) {
	GLint count;
	gl(GetProgramiv(program.handle, GL_ACTIVE_UNIFORMS, &count));
	return count;
}

inline int get_attribute_count(struct Program program) {
	GLint count;
	gl(GetProgramiv(program.handle, GL_ACTIVE_ATTRIBUTES, &count));
	return count;
}

inline void create_shader(struct Shader* shader, GLenum type) {
	shader->handle = gl(CreateShader(type));
}

inline void destroy_shader(struct Shader* shader) {
	gl(DeleteShader(shader->handle));
	shader->handle = (GLuint)(-1);
}

inline void create_program(struct Program* program) {
	program->handle = gl(CreateProgram());
}

inline void destroy_program(struct Program* program) {
	gl(DeleteProgram(program->handle));
	program->handle = (GLuint)(-1);
}

inline void set_attribute_handle(struct Program program, const char* id, GLuint handle) {
	gl(BindAttribLocation(program.handle, handle, id));
}

inline void bind_program(struct Program program) {
	gl(UseProgram(program.handle));
}

inline void unbind_program() {
	gl(UseProgram(0));
}

inline struct Uniform get_uniform(struct Program program, const char* id) {
	struct Uniform uniform;
	uniform.handle = gl(GetUniformLocation(program.handle, id));
	return uniform;
}

inline struct Attribute get_attribute(struct Program program, const char* id) {
	struct Attribute attribute;
	attribute.handle = gl(GetAttribLocation(program.handle, id));
	return attribute;
}

inline void enable_attribute(struct Attribute attribute) {
	gfx_assert(bound_vao().handle != (GLuint)(-1));
	gl(EnableVertexAttribArray(attribute.handle));
}

inline void disable_attribute(struct Attribute attribute) {
	gfx_assert(bound_vao().handle != (GLuint)(-1));
	gl(DisableVertexAttribArray(attribute.handle));
}

/* todo: access normalized boolean for performance and change it to GL_TRUE when GL_UNSIGNED_BYTE */
inline void set_attribute_ptr(struct Attribute attribute, GLenum type, GLuint components) {
	gl(VertexAttribPointer(attribute.handle, components, type, GL_FALSE, 0, (GLvoid*)NULL));
}

inline void set_attribute1f(struct Attribute attribute, float v) {
	gl(VertexAttrib1f(attribute.handle, v));
}

inline void set_attribute2f(struct Attribute attribute, const vec2 v) {
	gl(VertexAttrib2f(attribute.handle, v[0], v[1]));
}

inline void set_attribute3f(struct Attribute attribute, const vec3 v) {
	gl(VertexAttrib3f(attribute.handle, v[0], v[1], v[2]));
}

inline void set_attribute4f(struct Attribute attribute, const vec4 v) {
	gl(VertexAttrib4f(attribute.handle, v[0], v[1], v[2], v[3]));
}

inline void set_uniform1i(struct Uniform uniform, int v) {
	gl(Uniform1i(uniform.handle, v));
}

inline void set_uniform1f(struct Uniform uniform, float v) {
	gl(Uniform1f(uniform.handle, v));
}

inline void set_uniform2f(struct Uniform uniform, const vec2 v) {
	gl(Uniform2f(uniform.handle, v[0], v[1]));
}

inline void set_uniform3f(struct Uniform uniform, const vec3 v) {
	gl(Uniform3f(uniform.handle, v[0], v[1], v[2]));
}

inline void set_uniform4f(struct Uniform uniform, const vec4 v) {
	gl(Uniform4f(uniform.handle, v[0], v[1], v[2], v[3]));
}

inline void set_uniform4x4f(struct Uniform uniform, const mat4x4 v) {
	gl(UniformMatrix4fv(uniform.handle, 1, GL_FALSE, &v[0][0]));
}

#endif
