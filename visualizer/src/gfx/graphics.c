#include "gfx/graphics.h"
#include "utl/utilities.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* helper functions */
/* program is expected to be created already */
static int load_program(struct Program* program, const char* vert_src, const char* frag_src) {
	struct Shader vert, frag;
	create_shader(&vert, GL_VERTEX_SHADER);
	create_shader(&frag, GL_FRAGMENT_SHADER);

	/* attempt to compile each shader */
	char* vert_result = compile_shader(vert, vert_src);
	if(vert_result != NULL) {
		fprintf(stderr, "failed to load vertex shader:\n%s", vert_result);
		free(vert_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		return 0;
	}

	char* frag_result = compile_shader(frag, frag_src);
	if(frag_result != NULL) {
		fprintf(stderr, "failed to load fragment shader:\n%s", frag_result);
		free(frag_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		return 0;
	}

	/* attempt to link the result with program */
	char* link_result = link_program(*program, vert, frag);
	if(link_result != NULL) {
		fprintf(stderr, "failed to link program:\n%s", link_result);
		free(link_result);
		destroy_shader(&vert);
		destroy_shader(&frag);
		return 0;
	}

	return 1;
}

/* static variables */
static int static_initialized = 0;
/* render a front-facing quad using render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP); */
static struct Mesh2D quad;

int init_graphics() {
	/* init glfw */
	if(!glfwInit()) {
		fprintf(stderr, "failed to init glfw.\n");
		return 0;
	}
	
	/* note that we can't yet init the static variables since we don't have a context */
	return 1;
}

void terminate_graphics() {
	/* it's assumed that we have a state here */
	if(static_initialized) {
		/* destroy opengl objects */
		destroy_mesh2d(&quad);
	}

	/* kill any windows which are left open */
	glfwTerminate();
}

void reset_graphics(struct Graphics* graphics) {
	graphics->window = NULL;
	graphics->display.handle = (GLuint)(-1);
	graphics->frame.handle = (GLuint)(-1);
	memset(&graphics->frame_data[0][0][0], 0, sizeof(graphics->frame_data));
}

#define VERT_SRC "\
#version 330 \n\
uniform vec2 scale; \
uniform vec2 offset; \
in vec2 quad_cs; \
out vec2 frag_uv_ss; \
void main() { \
	frag_uv_ss = quad_cs.xy * 0.5 + 0.5; \
	gl_Position = vec4(vec3(quad_cs.xy * scale + offset, 0.0), 1.0); \
}"

#define FRAG_SRC "\
#version 330 \n\
uniform sampler2D blit_texture; \
uniform vec2 uv_offset; \
uniform vec2 uv_scale; \
uniform vec4 color; \
in vec2 frag_uv_ss; \
void main() { \
	gl_FragColor = texture(blit_texture, frag_uv_ss * uv_scale + uv_offset) * color; \
}"

int setup_graphics(struct Graphics* graphics) {
	if(graphics->window == NULL) {
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		/* create the glfw window before gl calls */
		graphics->window = glfwCreateWindow(2 * GFX_DAVIS_FX2_X, 2 * GFX_DAVIS_FX2_Y, "External Visualizer", NULL, NULL);
		if(graphics->window == NULL) {
			fprintf(stderr, "failed to open glfw window. is opengl 3.3 supported?\n");
			return 0;
		}
		glfwPollEvents();
		glfwMakeContextCurrent(graphics->window);

		/* initialize the opengl context */
		if(gl3wInit() != 0) {
			fprintf(stderr, "failed to initialize opengl.\n");
			return 0;
		}

		/* make sure 3.3 is supported */
		if(!gl3wIsSupported(3, 3)) {
			fprintf(stderr, "opengl 3.3 is not supported.\n");
			return 0;
		}

		/* now that we have a context, initialize our static variables */
		if(!static_initialized) {
			/* init static variables here */
			create_mesh2d(&quad);
			quad.vertices_size = 4;
			quad.vertices = malloc(sizeof(*quad.vertices) * quad.vertices_size);
			if(quad.vertices == NULL) {
				/* make sure to delete the opengl objects */
				destroy_mesh2d(&quad);
				return 0;
			}

			/* copy the vertices into dynamic memory */
			static vec2 quad_data[4] = {
				{ -1.0, -1.0 },
				{  1.0, -1.0 },
				{ -1.0,  1.0 },
				{  1.0,  1.0 }
			};
			memcpy(quad.vertices, quad_data, sizeof(quad_data));
			
			/* quad.vertices is free'd */
			upload_mesh2d(&quad);

			/* only run this code once */
			static_initialized = 1;
		}

		/* create opengl objects here */
		create_program(&graphics->display);
		create_texture2d(&graphics->frame);

		/* since everything has been created if we have to bail, make sure to goto cleanup_fail */
		/* todo: figure out a way to get decent errors without all of this string duplication */
		const char* err = "";

		/* load the display program only once since it's static */
		if(!load_program(&graphics->display, VERT_SRC, FRAG_SRC)) { err = "display program"; goto cleanup_fail; }
		
		/* load the display's attributes and uniforms */
		/* make sure there are the same # of attributes and uniforms as expected */
		if(get_attribute_count(graphics->display) != 1) { err = "attribute count"; goto cleanup_fail; }
		if(get_uniform_count(graphics->display) != 6) { err = "uniform count"; goto cleanup_fail; }
		graphics->quad_cs      = get_attribute(graphics->display, "quad_cs");
		graphics->scale        = get_uniform(graphics->display, "scale");
		graphics->offset       = get_uniform(graphics->display, "offset");
		graphics->blit_texture = get_uniform(graphics->display, "blit_texture");
		graphics->uv_offset    = get_uniform(graphics->display, "uv_offset");
		graphics->uv_scale     = get_uniform(graphics->display, "uv_scale");
		graphics->color        = get_uniform(graphics->display, "color");
		if(graphics->quad_cs.handle      == (GLuint)(-1)) { err = "quad_cs"; goto cleanup_fail; }
		if(graphics->scale.handle        == (GLuint)(-1)) { err = "scale"; goto cleanup_fail; }
		if(graphics->offset.handle       == (GLuint)(-1)) { err = "offset"; goto cleanup_fail; }
		if(graphics->blit_texture.handle == (GLuint)(-1)) { err = "blit_texture"; goto cleanup_fail; }
		if(graphics->uv_offset.handle    == (GLuint)(-1)) { err = "uv_offset"; goto cleanup_fail; }
		if(graphics->uv_scale.handle     == (GLuint)(-1)) { err = "uv_scale"; goto cleanup_fail; }
		if(graphics->color.handle        == (GLuint)(-1)) { err = "color"; goto cleanup_fail; }

		/* now that the shader is loaded, we need to specify what attribute a mesh effects */
		bind_program(graphics->display);
		attach_mesh2d(&quad, (struct Attribute*){ &graphics->quad_cs }, 1);
		unbind_program();

		goto skip_cleanup_fail;
cleanup_fail:
		fprintf(stderr, "error code %s.\n", err);
		setdown_graphics(graphics);
		return 0;
	}
skip_cleanup_fail:

	/* static opengl settings */
	gl(Enable(GL_CULL_FACE)); /* cull back faces */
	gl(CullFace(GL_BACK));    /* ... */
	gl(Disable(GL_BLEND));    /* disable blending, we dont need it.. yet */
	gl(ClearColor(0.0, 0.0, 0.0, 1.0));

	/* load frame texture data onto the graphics card */
	bind_texture2d(graphics->frame);
	set_filter_texture2d(GL_NEAREST);
	set_wrap_texture2d(GL_CLAMP_TO_EDGE);
	unbind_texture2d();

	/* load default frame texture data */
	memset(&graphics->frame_data[0][0][0], 0, 3 * GFX_DAVIS_FX2_X * GFX_DAVIS_FX2_Y);
	graphics_update_frame(graphics);

	return 1;
}

void setdown_graphics(struct Graphics* graphics) {
	destroy_program(&graphics->display);
	destroy_texture2d(&graphics->frame);

	/* don't destroy window before gl calls */
	glfwDestroyWindow(graphics->window);
}

void graphics_update_frame(struct Graphics* graphics) {
	bind_texture2d(graphics->frame);
	/* by default opengl expects the # of bytes in each row to be a multiple of 4 */
	/* let opengl know that graphics->frame_data doesn't have such padding */
	gl(PixelStorei(GL_UNPACK_ALIGNMENT, 1));
	upload_texture2d(&graphics->frame_data[0][0][0], GFX_DAVIS_FX2_X, GFX_DAVIS_FX2_Y, GL_UNSIGNED_BYTE, GL_RGB, GL_RGB8);
	unbind_texture2d();
}

int render_graphics(struct Graphics* graphics) {
	/* incase the window settings have changed */
	int width, height;
	glfwGetFramebufferSize(graphics->window, &width, &height);
	static int last_width = 0, last_height = 0;
	if(last_width != width || last_height != height) {
		gl(Viewport(0, 0, width, height));
	}
	last_width = width;
	last_height = height;

	/* clear the back buffer */
	gl(Clear(GL_COLOR_BUFFER_BIT));
	
	bind_program(graphics->display);
	set_uniform2f(graphics->scale, (vec2){ 1.0, 1.0 }); 
	set_uniform2f(graphics->offset, (vec2){ 0.0, 0.0 }); 
	set_uniform2f(graphics->uv_scale, (vec2){ 1.0, 1.0 });
	set_uniform2f(graphics->uv_offset, (vec2){ 0.0, 0.0 });
	set_uniform4f(graphics->color, (vec4){ 1.0, 1.0, 1.0, 1.0 });
	set_uniform1i(graphics->blit_texture, active_texture(0));
	bind_texture2d(graphics->frame);
	render_mesh2d_vertices(&quad, GL_TRIANGLE_STRIP);
	unbind_texture2d();
	unbind_program();

	/* display and poll for window events */
	glfwSwapBuffers(graphics->window);
	glfwPollEvents();

	return 1;
}
