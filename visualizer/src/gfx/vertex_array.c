#include "gfx/vertex_array.h"

extern inline void create_vao(struct Vao* vao);
extern inline void destroy_vao(struct Vao* vao);
extern inline void bind_vao(struct Vao vao);
extern inline void unbind_vao();
extern inline struct Vao bound_vao();
