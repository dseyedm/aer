#include "gfx/texture.h"

extern inline int active_texture(GLint unit);

extern inline void create_texture2d(struct Texture2D* texture);
extern inline void destroy_texture2d(struct Texture2D* texture);

extern inline void bind_texture2d(struct Texture2D texture);
extern inline void unbind_texture2d();
extern inline struct Texture2D bound_texture2d();

extern inline void upload_texture2d(GLvoid* data, GLuint width, GLuint height, GLenum type, GLenum format, GLenum internal_format);

extern inline void set_filter_texture2d(GLenum filter_mode);
extern inline void set_wrap_texture2d(GLenum wrap_mode);
