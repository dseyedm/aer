#ifndef GFX_MESH_H
#define GFX_MESH_H

#include "gfx/vertex_array.h"
#include "gfx/vertex_buffer.h"
#include "gfx/shader.h"

#include <linmath/linmath.h>
#include <stdint.h>

struct Mesh2D {
	/* ommitted normals, tangents, bitangents */
	/* opengl objects */
	struct Vao vao;
	struct Vbo elements_vbo,
	           vertices_vbo,
	           uvs_vbo;

	/* mesh data */
	uint8_t* elements; /* there shouldn't be more than 255 vertices in a 2d mesh */
	vec2* vertices;
	vec2* uvs;

	size_t elements_size, vertices_size, uvs_size;
};

void create_mesh2d(struct Mesh2D* mesh);
void destroy_mesh2d(struct Mesh2D* mesh);

void reset_mesh2d(struct Mesh2D* mesh);

/* any non NULL mesh data is free'd */
void upload_mesh2d(struct Mesh2D* mesh);
/* list order is: vertex, uvs; ignore any attributes with handle = (GLuint)(-1) */
void attach_mesh2d(struct Mesh2D* mesh, struct Attribute* list, size_t list_size);

void render_mesh2d_elements(struct Mesh2D* mesh, GLenum mode);
void render_mesh2d_vertices(struct Mesh2D* mesh, GLenum mode);

/* ommitted Mesh3D */

#endif
