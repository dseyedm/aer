/* 
	soraj.seyedmahmoud@uleth.ca
	before running this program, remember to
	set cAER to output polarity via udp and frame, imu, and special data via tcp
	tested with cAER r7040 
 */

#include "gfx/graphics.h"
#include "net/networking.h"
#include "../../caer/log_common.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

int parse_port(const char* port_str) {
	int port;
	if(sscanf(port_str, "%d", &port) != 1 || port < 0) {
		fprintf(stderr, "failed to parse port \"%s\"\n", port_str);
		return 0;
	}
	return port;
}

int main(int argc, char *argv[]) {
	FILE* log_file = NULL;
	/* read network settings */
	const char* caer_ip = "127.0.0.1";
	int caer_udp_port = 8888;
	if(argc == 3) {
		caer_ip = argv[1];
		caer_udp_port = parse_port(argv[2]);
		if(!caer_udp_port) return 0;
	} else if(argc == 2) {
		log_file = fopen(argv[1], "rb");
		if(!log_file) {
			goto usage;
		}
	} else { 
		goto usage;
	} 

	struct Net net;
	file_header_t file_header;
	if(log_file) {
		fread(&file_header, sizeof(file_header), 1, log_file);
		if(feof(log_file) || file_header.version != LOG_VERSION) {
			fprintf(stderr, "invalid log file version %d\n", file_header.version);
			fclose(log_file);
			goto usage;
		}
	} else {
		/* connect to caer using the data in net */
		if(!setup_net(&net, caer_ip, caer_udp_port)) {
			return 0;
		}
	}
	
	/* initialize graphics */
	if(!init_graphics()) {
		if(log_file) {
			fclose(log_file);
		} else {
			setdown_net(&net);
		}
		return 0;
	}

	/* setup graphics and render a blank frame */
	struct Graphics graphics;
	reset_graphics(&graphics);
	if(!setup_graphics(&graphics)) {
		terminate_graphics();
		if(log_file) {
			fclose(log_file);
		} else {
			setdown_net(&net);
		}
		return 0;
	}

	if(!render_graphics(&graphics)) {
		setdown_graphics(&graphics);
		terminate_graphics(&graphics);
		if(log_file) {
			fclose(log_file);
		} else {
			setdown_net(&net);
		}
		return 0;
	}

	if(log_file) {
		// do one linear scan to find n packets of each type, and max events within each type
		size_t npolarity_packets = 0, nframe_packets = 0, nimu6_packets = 0;
		size_t max_polarity_events = 0, max_frame_events = 0, max_imu6_events = 0;
		{
			fseek(log_file, sizeof(file_header), SEEK_SET);
			event_header_t header;
			while(1) {
				int64_t current = ftell(log_file);
				assert(current >= 0);
				fread(&header, sizeof(header), 1, log_file);
				if(feof(log_file)) break;
				current += sizeof(header);
				int good = 1;
				switch(header.type) {
					case LOG_TYPE_POLARITY: {
						++npolarity_packets;
						max_polarity_events = (max_polarity_events >= header.nevents ? max_polarity_events : header.nevents);
						fseek(log_file, current + header.nevents * sizeof(polarity_event_t), SEEK_SET);
					} break;
					case LOG_TYPE_FRAME: {
						++nframe_packets;
						max_frame_events = (max_frame_events >= header.nevents ? max_frame_events : header.nevents);
						fseek(log_file, current + header.nevents * sizeof(frame_event_t), SEEK_SET);
					} break;
					case LOG_TYPE_IMU6: {
						++nimu6_packets;
						max_imu6_events = (max_imu6_events >= header.nevents ? max_imu6_events : header.nevents);
						fseek(log_file, current + header.nevents * sizeof(imu6_event_t), SEEK_SET);
					} break;
					default: good = 0;
				}
				if(!good) break;
			}
		}

		// do another linear scan to record all of the packet offsets
		size_t polarity_offset[npolarity_packets], frame_offset[nframe_packets], imu6_offset[nimu6_packets];
		{
			fseek(log_file, sizeof(file_header), SEEK_SET);
			event_header_t header;
			size_t polarity_packet_i = 0, frame_packet_i = 0, imu6_packet_i = 0;
			while(1) {
				int64_t current = ftell(log_file);
				assert(current >= 0);
				fread(&header, sizeof(header), 1, log_file);
				if(feof(log_file)) break;
				//current += sizeof(header);
				int good = 1;
				switch(header.type) {
					case LOG_TYPE_POLARITY: {
						assert(polarity_packet_i < npolarity_packets);
						polarity_offset[polarity_packet_i] = current;
						current += sizeof(header);
						++polarity_packet_i;
						fseek(log_file, current + header.nevents * sizeof(polarity_event_t), SEEK_SET);
					} break;
					case LOG_TYPE_FRAME: {
						assert(frame_packet_i < nframe_packets);
						frame_offset[frame_packet_i] = current;
						current += sizeof(header);
						++frame_packet_i;
						fseek(log_file, current + header.nevents * sizeof(frame_event_t), SEEK_SET);
					} break;
					case LOG_TYPE_IMU6: {
						assert(imu6_packet_i < nimu6_packets);
						imu6_offset[imu6_packet_i] = current;
						current += sizeof(header);
						++imu6_packet_i;
						fseek(log_file, current + header.nevents * sizeof(imu6_event_t), SEEK_SET);
					} break;
					default: good = 0;
				}
				if(!good) break;
			}
		}

		(void)(imu6_offset);
		(void)(frame_offset);
		(void)(max_frame_events);
		(void)(max_imu6_events);

		assert(npolarity_packets || nframe_packets || nimu6_packets);

		size_t polarity_packet_i = 0;//, frame_packet_i = 0, imu6_packet_i = 0;
		size_t polarity_event_i = 0;//, frame_event_i = 0, imu6_event_i = 0;
		event_header_t polarity_header;
		polarity_event_t polarity_events[max_polarity_events];

		fseek(log_file, polarity_offset[polarity_packet_i], SEEK_SET);
		fread(&polarity_header, 1, sizeof(polarity_header), log_file);
		fread(&polarity_events[0], polarity_header.nevents, sizeof(polarity_events[0]), log_file);

		double update_s = 0.016; // update @ 60 fps
		uint32_t ts_per_update = update_s / (1.0 / 10000) * 100; // timestamps are 1/10000s

		glfwSetTime(0.0);
		while(!glfwWindowShouldClose(graphics.window)) {
			if(glfwGetTime() < update_s) continue;
			glfwSetTime(0.0);

			uint32_t diff_ts = 0, last_ts = polarity_events[polarity_event_i].timestamp;
			while(diff_ts < ts_per_update && glfwGetTime() < update_s) {
				if(polarity_event_i >= polarity_header.nevents) {
					last_ts = polarity_events[polarity_event_i].timestamp;
					polarity_event_i = 0;
					polarity_packet_i = (polarity_packet_i + 1) % npolarity_packets;
					fseek(log_file, polarity_offset[polarity_packet_i], SEEK_SET);
					fread(&polarity_header, 1, sizeof(polarity_header), log_file);
					fread(&polarity_events[0], polarity_header.nevents, sizeof(polarity_events[0]), log_file);
				} 
				uint32_t i = polarity_event_i;
				if(i) {
					if(polarity_events[i].timestamp < polarity_events[i-1].timestamp) {
						diff_ts += (uint32_t)(-1) - (polarity_events[i-1].timestamp - polarity_events[i].timestamp);
					} else {
						// note that we assume that if ts0 < ts1, that there hasn't been an overlap in the time between ts0 and ts1
						diff_ts += polarity_events[i].timestamp - polarity_events[i-1].timestamp;
					}
				} else {
					if(polarity_events[i].timestamp < last_ts) {
						diff_ts += (uint32_t)(-1) - (last_ts - polarity_events[i].timestamp);
					} else {
						diff_ts += polarity_events[i].timestamp - last_ts;
					}
				}
				assert(polarity_events[i].x < GFX_DAVIS_FX2_X && polarity_events[i].y < GFX_DAVIS_FX2_Y);
				graphics.frame_data[polarity_events[i].y][polarity_events[i].x][2] = 0;
				graphics.frame_data[polarity_events[i].y][polarity_events[i].x][0] |= !polarity_events[i].polarity * 255;
				graphics.frame_data[polarity_events[i].y][polarity_events[i].x][1] |= !!polarity_events[i].polarity * 255;
				++polarity_event_i;
			}
			//printf("%u ", diff_ts);

			graphics_update_frame(&graphics);
			memset(graphics.frame_data, 0, sizeof(graphics.frame_data));
			if(!render_graphics(&graphics)) break;
		}
	} else {
		clock_t current = clock();
		int udp_update_ms = 2;
		clock_t udp_start = current;

		while(!glfwWindowShouldClose(graphics.window)) {
			/* update timers */
			current = clock();
			int udp_ms = (current - udp_start) * 1000 / CLOCKS_PER_SEC;

			/* sync the udp thread with the graphics state */
			if(udp_ms >= udp_update_ms && udp_packet_count(&net) >= 1) {
				sync_udp_with_graphics(&net, &graphics);
				graphics_update_frame(&graphics);

				/* render the current state */
				if(!render_graphics(&graphics)) break;

				/* reset the frame before the next sync */
				memset(graphics.frame_data, 0, sizeof(graphics.frame_data));

				/* reset the timer */
				udp_start = current;
			}
		}
	}

	setdown_graphics(&graphics);
	terminate_graphics();
	if(log_file) {
		fclose(log_file);
	} else {
		setdown_net(&net);
	}

	return 0;

usage:
	/* invalid # of arguments, either pass <log_file>, or <ip> <port>, or nothing */
	printf("log version %d\n%s [<log file> | [<ip> <udp_port>]]\n", LOG_VERSION, argv[0]);
	return 0;
}
