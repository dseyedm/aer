#ifndef UTL_UTILITIES_H
#define UTL_UTILITIES_H

/* caller must free returned string */
char* load_file(const char* file);

#endif
