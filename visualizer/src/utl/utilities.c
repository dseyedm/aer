#include "utl/utilities.h"

#include <stdio.h>
#include <stdlib.h>

char* load_file(const char* file) {
	FILE* fp = fopen(file, "rb");
	if(fp == NULL) {
		fprintf(stderr, "failed to load \"%s\"", file);
		return NULL;
	}

	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	char* contents = malloc(sizeof(*contents) * size + 1);
	if(contents == NULL) return NULL;

	fread(contents, size, 1, fp);

	return contents;
}
