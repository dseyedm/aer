Screenshot
----------
![visualizer.png](https://bitbucket.org/sorajsm/aer/downloads/visualizer.png)

Description
-----------
The visualizer software displays data sent by a cAER server.

Credits
-------
Project credits are stored in ~/README

Licensing
---------
Licensing information is stored in ~/README