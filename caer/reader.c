#include "log_common.h"

#include <assert.h>

void print_event_header(event_header_t* header);
void print_polarity_event(polarity_event_t* event);
void print_frame_event(frame_event_t* event);
void print_imu6_event(imu6_event_t* event);

int main(int argc, char** argv) {
	FILE* log_file = NULL;

	if(argc != 2) goto usage;
	log_file = fopen(argv[1], "rb");
	if(log_file == NULL) goto usage;

	file_header_t file_header;
	fread(&file_header, sizeof(file_header), 1, log_file);
	if(file_header.version != LOG_VERSION) {
		printf("invalid log file version %d\n", file_header.version);
		fclose(log_file);
		goto usage;
	}

	int cunt = 3;
	while(cunt--) {
		event_header_t header;
		fread(&header, sizeof(header), 1, log_file);
		if(feof(log_file)) break;
		print_event_header(&header);

		switch(header.type) {
			case LOG_TYPE_POLARITY: {
				polarity_event_t events[header.nevents];
				fread(&events[0], sizeof(events[0]), header.nevents, log_file);
				long int currPosition = ftell(log_file);
				printf("curr pos: %lld\n", currPosition);
				if(feof(log_file)) break;
				for(uint32_t i = 0; i < header.nevents; ++i) {
					print_polarity_event(&events[i]);
					if(i >= 4) break;
				}
			} break;
			case LOG_TYPE_FRAME: {
				frame_event_t events[header.nevents];
				fread(&events[0], sizeof(events[0]), header.nevents, log_file);
				if(feof(log_file)) break;
				for(uint32_t i = 0; i < header.nevents; ++i) {
					print_frame_event(&events[i]);
				}
			} break;
			case LOG_TYPE_IMU6: {
				imu6_event_t events[header.nevents];
				fread(&events[0], sizeof(events[0]), header.nevents, log_file);
				if(feof(log_file)) break;
				for(uint32_t i = 0; i < header.nevents; ++i) {
					print_imu6_event(&events[i]);
				}
			} break;
		}
	}

	fclose(log_file);
	return 0;

usage:
	printf("version %d\n", LOG_VERSION);
	printf("%s <log file>\n", argv[0]);
	return 0;
}
