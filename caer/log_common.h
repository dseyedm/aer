#ifndef LOG_COMMON_H
#define LOG_COMMON_H

#include "events/common.h"
#include "events/polarity.h"
#include "events/frame.h"
#include "events/imu6.h"
#include "events/special.h"

#include <stdint.h>

#define LOG_VERSION 0

typedef struct {
	uint8_t version;
} file_header_t;

#define LOG_TYPE_POLARITY 0
#define LOG_TYPE_FRAME 1
#define LOG_TYPE_IMU6 2

typedef struct {
	uint8_t type;
	uint32_t nevents;
} event_header_t;

typedef struct {
	uint8_t x;
	uint8_t y;
	uint8_t polarity;
	uint32_t timestamp;
} polarity_event_t;

#define LOG_PIXELS_X 240
#define LOG_PIXELS_Y 180

typedef struct {
	uint32_t ts_frame_start;
	uint32_t ts_frame_end;
	uint32_t ts_exposure_start;
	uint32_t ts_exposure_end;
	uint16_t pixels[LOG_PIXELS_Y][LOG_PIXELS_X];
} frame_event_t;

typedef struct {
	float acceleration[3];
	float gyrometer[3];
	float temperature;
	uint32_t timestamp;
} imu6_event_t;

inline void print_event_header(event_header_t* header) {
	printf("EVT %u %u\n", header->type, header->nevents);
}

inline void print_polarity_event(polarity_event_t* event) {
	printf("POL %u %u %u %u\n", event->x, event->y, event->polarity, event->timestamp);
}

inline void print_frame_event(frame_event_t* event) {
	printf("FRM %u %u %u %u\n", event->ts_frame_start, event->ts_frame_end, event->ts_exposure_start, event->ts_exposure_end);
}

inline void print_imu6_event(imu6_event_t* event) {
	printf("IMU %f %f %f %f %f %f %f %u\n", event->acceleration[0], event->acceleration[1], event->acceleration[2], event->gyrometer[0], event->gyrometer[1], event->gyrometer[2], event->temperature, event->timestamp);
}

#endif
