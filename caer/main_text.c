/* 
	soraj seyed mahmoud edit: writes data into a file specified in argv 
*/

#include "main.h"
#include "base/config.h"
#include "base/config_server.h"
#include "base/mainloop.h"
#include "base/misc.h"
#include "modules/ini/dvs128.h"
#include "modules/ini/davis_fx2.h"
#include "modules/ini/davis_fx3.h"
#include "modules/backgroundactivityfilter/backgroundactivityfilter.h"
#include "modules/statistics/statistics.h"
//#include "modules/visualizer/visualizer.h"
#include "modules/misc/out/net_tcp_server.h"
#include "modules/misc/out/net_udp.h"
#include "log_common.h"

#include <assert.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>

int kbhit(void) {
	struct termios oldt, newt;
	int ch;
	int oldf;

	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

	ch = getchar();

	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	fcntl(STDIN_FILENO, F_SETFL, oldf);

	if(ch != EOF) {
		ungetc(ch, stdin);
		return 1;
	}
	return 0;
}

static FILE* log_file = NULL;

#define UNUSED(x) (void)(x)

static bool writerOutputFile(
	caerPolarityEventPacket polarity,
	caerFrameEventPacket frame,
	caerIMU6EventPacket imu,
	caerSpecialEventPacket special) 
{
	UNUSED(special);

#if 1
	if(polarity != NULL && polarity->packetHeader.eventType == POLARITY_EVENT) {
		uint32_t i, nevents = caerEventPacketHeaderGetEventNumber(&polarity->packetHeader);
		for(i = 0; i < nevents; ++i) {
			caerPolarityEvent event = caerPolarityEventPacketGetEvent(polarity, i);
			if(!caerPolarityEventIsValid(event)) continue;
			fprintf(log_file, "%u %u %u %u\n", (uint8_t)caerPolarityEventGetX(event), (uint8_t)caerPolarityEventGetY(event), caerPolarityEventGetPolarity(event), caerPolarityEventGetTimestamp(event));
		}
	}
#endif

#if 0
	if(frame != NULL && frame->packetHeader.eventType == FRAME_EVENT) {
		event_header_t header;
		header.type = LOG_TYPE_FRAME;
		header.nevents = 0;
		uint32_t nevents = caerEventPacketHeaderGetEventNumber(&frame->packetHeader), i;
		frame_event_t frame_events[nevents];
		for(i = 0; i < nevents; ++i) {
			caerFrameEvent event = caerFrameEventPacketGetEvent(frame, i);
			if(!caerFrameEventIsValid(event)) continue;
			frame_events[header.nevents].ts_frame_start = caerFrameEventGetTSStartOfFrame(event);
			frame_events[header.nevents].ts_frame_end = caerFrameEventGetTSEndOfFrame(event);
			frame_events[header.nevents].ts_exposure_start = caerFrameEventGetTSStartOfExposure(event);
			frame_events[header.nevents].ts_exposure_end = caerFrameEventGetTSEndOfExposure(event);
			assert(caerFrameEventGetLengthX(event) == LOG_PIXELS_X && 
			       caerFrameEventGetLengthY(event) == LOG_PIXELS_Y && 
			       caerFrameEventGetChannelNumber(event) == 1);
			for(uint16_t y = 0; y < LOG_PIXELS_Y; ++y) {
				for(uint16_t x = 0; x < LOG_PIXELS_X; ++x) {
					frame_events[header.nevents].pixels[y][x] = caerFrameEventGetPixel(event, x, y);
				}
			}
			++header.nevents;
		}
		if(header.nevents) {
			fwrite(&header, sizeof(header), 1, log_file);
			fwrite(frame_events, sizeof(frame_events[0]), header.nevents, log_file);
		}
	}
#endif

#if 0
	if(imu != NULL && imu->packetHeader.eventType == IMU6_EVENT) {
		event_header_t header;
		header.type = LOG_TYPE_IMU6;
		header.nevents = 0;
		uint32_t nevents = caerEventPacketHeaderGetEventNumber(&imu->packetHeader), i;
		imu6_event_t imu_events[nevents];
		for(i = 0; i < nevents; ++i) {
			caerIMU6Event event = caerIMU6EventPacketGetEvent(imu, i);
			if(!caerIMU6EventIsValid(event)) continue;
			imu_events[header.nevents].acceleration[0] = caerIMU6EventGetAccelX(event);
			imu_events[header.nevents].acceleration[1] = caerIMU6EventGetAccelY(event);
			imu_events[header.nevents].acceleration[2] = caerIMU6EventGetAccelZ(event);
			imu_events[header.nevents].gyrometer[0] = caerIMU6EventGetGyroX(event);
			imu_events[header.nevents].gyrometer[1] = caerIMU6EventGetGyroY(event);
			imu_events[header.nevents].gyrometer[2] = caerIMU6EventGetGyroZ(event);
			imu_events[header.nevents].temperature = caerIMU6EventGetTemp(event);
			imu_events[header.nevents].timestamp = caerIMU6EventGetTimestamp(event);
			++header.nevents;
		}
		if(header.nevents) {
			fwrite(&header, sizeof(header), 1, log_file);
			fwrite(imu_events, sizeof(imu_events[0]), header.nevents, log_file);
		}
	}
#endif
	return true;
}

clock_t start;

static bool mainloop_1(void) {
	// Typed EventPackets contain events of a certain type.
	caerPolarityEventPacket polarity;
	caerFrameEventPacket frame;
	caerIMU6EventPacket imu;
	caerSpecialEventPacket special;

	// Input modules grab data from outside sources (like devices, files, ...)
	// and put events into an event packet.
	caerInputDAVISFX2(1, &polarity, &frame, &imu, &special);

	// Filters process event packets: for example to suppress certain events,
	// like with the Background Activity Filter, which suppresses events that
	// look to be uncorrelated with real scene changes (noise reduction).
	caerBackgroundActivityFilter(2, polarity);

	// Filters can also extract information from event packets: for example
	// to show statistics about the current event-rate.
	caerStatistics(3, (caerEventPacketHeader) polarity, 1000);

	// Broadcast polarity events via UDP
	caerOutputNetUDP(5, 1, polarity);

	static bool stop = false;
	static bool last_keyhit = false;
	bool keyhit = kbhit();

	clock_t now = clock();
	static bool good = false;
	if(!good) {
		good = (now - start) / CLOCKS_PER_SEC > 0.2;
		if(good) {
			struct timeval tv;
			gettimeofday(&tv, NULL);
			unsigned long time_in_micros = 1000000 * tv.tv_sec + tv.tv_usec;
			printf("\nstarted logging @ %llu.\n", time_in_micros);
			fprintf(log_file, "%llu\n", time_in_micros);
		}
	}

	if(!stop && good) {
		writerOutputFile(polarity, frame, imu, special);
	} 

	if(keyhit && !last_keyhit) { 
		printf("\nstopped logging.\n");
		fclose(log_file);
		stop = true;
		exit(0);
		return false;
	} 
	last_keyhit = keyhit;

	return true;
}

int main(int argc, char** argv) {
	if(argc != 2) goto usage;
	log_file = fopen(argv[1], "wb");
	if(log_file == NULL) goto usage;

	file_header_t file_header;
	file_header.version = LOG_VERSION;
	//fwrite(&file_header, sizeof(file_header), 1, log_file);

	// Initialize config storage from file, support command-line overrides.
	// If no init from file needed, pass NULL.
	caerConfigInit("caer-config.xml", 0, NULL);

	// Initialize logging sub-system.
	caerLogInit();

	// Daemonize the application (run in background).
	//caerDaemonize();

	start = clock();
	// Start the configuration server thread for run-time config changes.
	caerConfigServerStart();

	// Finally run the main event processing loops.
	struct caer_mainloop_definition mainLoops[1] = { { 1, &mainloop_1 } };
	caerMainloopRun(&mainLoops, 1); // Only start Mainloop 1.

	// After shutting down the mainloops, also shutdown the config server
	// thread if needed.
	caerConfigServerStop();

	return (EXIT_SUCCESS);

usage:
	printf("%s <log file>\n", argv[0]);
	return EXIT_SUCCESS;
}
